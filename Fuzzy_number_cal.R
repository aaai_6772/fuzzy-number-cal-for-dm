# Calibrar creates three types of fuzzy numbers depending on the
# number of observations (node scores) describing each variable (condition):
# Trapezoidal fuzzy numbers
# Triangular fuzzy numbers
# Interval fuzzy numbers
calibrar <- function(md){

    ca<-sort(md)
    le<-length(md)
    
    if(le>=4){
    re<-ca[-1]
    re<-ca[-le]
    sa<-sort(sample(re,2))
    fn<-TrapezoidalFuzzyNumber(ca[1],sa[1],sa[2],ca[le])
    } else if(le==3){
    fn<-TriangularFuzzyNumber(ca[1],ca[2],ca[3])
    } else{
    fn<-as.FuzzyNumber(c(ca[1],ca[2]))
    }

    va<-value(fn)*10
    
    return(ca[le]/va)
    
}

#### Running the code ####

# Load packages
pac<-c('QCA', 'FuzzyNumbers')
lapply(pac, library, character.only = TRUE)

# Putting all together
set.seed(0)
data <- read.csv("np.csv", header = TRUE, sep = ",", row.names=1)
con <- read.csv("qca_conditions2.csv", header = TRUE, sep = ",", stringsAsFactors=FALSE, row.names=1)

conditions = dim(con)[1]
projects = dim(data)[2]

output <- matrix(ncol=projects, nrow=conditions)

for(c in seq(1,conditions)){
md<-data[rownames(data) %in% con[c,],]
for(p in seq(1,projects)){
output[c,p]<-calibrar(md[,p])
}
}

colnames(output) <- colnames(data)
rownames(output)<- rownames(con)
q_matrix <- t(output)

# Definition of hypothesis as a combination of conditions
h1<-c('B', 'C', 'D', 'E', 'F', 'H', 'OE')
H1<-data.frame(q_matrix[,h1])

# Necessity Analysis: Runned in terms of outcome
superSubset(H1, outcome ='OE', relation = "necessity", cov.cut = 0.6)

# Truth-tabling
t1<-truthTable(H1, outcome = "OE", incl.cut = 0.6, n.cut = 1, pri.cut = 0,exclude = NULL, 
               complete = FALSE, use.letters = FALSE,show.cases = TRUE)
